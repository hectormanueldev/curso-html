# Curso de HTML

## Temario

1. :heavy_plus_sign: Títulos y descripciones
1. :heavy_plus_sign: Encabezados
1. :heavy_plus_sign: Etiquetas de texto básicas
1. :heavy_plus_sign: Etiquetas de texto semánticas
1. :heavy_plus_sign: Etiquetas de salto
1. :heavy_plus_sign: Etiquetas de formateo
1. :heavy_plus_sign: Etiquetas semántica estructurales
1. :heavy_plus_sign: Etiquetas de línea y de bloque
1. :heavy_plus_sign: Imágenes
1. :heavy_plus_sign: Vectores
1. :heavy_plus_sign: Figuras
1. :heavy_plus_sign: Listas ordenadas
1. :heavy_plus_sign: Listas desordenadas
1. :heavy_plus_sign: Listas de definición
1. :heavy_plus_sign: Tablas
1. :heavy_plus_sign: Enlaces
1. :heavy_plus_sign: Enlaces internos
1. :heavy_plus_sign: Enlaces y protocolos especiales
1. :heavy_plus_sign: Elementos interactivos
1. :heavy_plus_sign: Audio y video
1. :heavy_plus_sign: iFrames
1. :heavy_plus_sign: Elementos de formularios
1. :heavy_plus_sign: Atributos de inputs y formularios
1. :heavy_plus_sign: Selects, radios y checkboxs
1. :heavy_plus_sign: Formulario de contacto
1. :heavy_plus_sign: Data attributes
1. :heavy_plus_sign: Metaetiquetas para seo y móviles
1. :heavy_plus_sign: Metaetiquetas para redes sociales
1. :heavy_plus_sign: Accesibilidad web
